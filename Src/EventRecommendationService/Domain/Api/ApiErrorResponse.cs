﻿namespace EventRecommendationService.Domain.Api
{
    using Newtonsoft.Json;

    public class ApiErrorResponse
    {
        public int StatusCode { get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; }

        public ApiErrorResponse(int statusCode, string message = null)
        {
            this.StatusCode = statusCode;
            this.Message = message ?? this.GetDefaultMessage(statusCode);
        }

        private string GetDefaultMessage(int statusCode)
        {
            switch (statusCode)
            {
                case 400:
                    return "Bad Request";
                case 404:
                    return "Item not found";
                case 500:
                    return "An unhandled error occured";
                default:
                    return null;
            }
        }
    }
}
