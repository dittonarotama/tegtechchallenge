﻿namespace EventRecommendationService.Domain.Api
{
    using System.Net.Http;
    using Polly.Retry;

    public interface IEventRecommendationPolicyWrapper
    {
        RetryPolicy<HttpResponseMessage> Policy { get; }

        void Build();
    }
}