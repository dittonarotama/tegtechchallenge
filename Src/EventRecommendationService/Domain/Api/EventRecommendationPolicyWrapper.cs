﻿namespace EventRecommendationService.Domain.Api
{
    using System;
    using System.Net.Http;
    using Microsoft.Extensions.Logging;
    using Polly;
    using Polly.Retry;

    public class EventRecommendationPolicyWrapper : IEventRecommendationPolicyWrapper
    {
        private readonly ILogger<EventRecommendationPolicyWrapper> logger;

        public EventRecommendationPolicyWrapper(ILogger<EventRecommendationPolicyWrapper> logger)
        {
            this.logger = logger;
        }

        public RetryPolicy<HttpResponseMessage> Policy { get; private set; }

        public void Build()
        {
            var retryPolicy = Polly.Policy.HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                            .WaitAndRetryAsync(new[]
                                                {
                                                    TimeSpan.FromSeconds(1),
                                                    TimeSpan.FromSeconds(2),
                                                    TimeSpan.FromSeconds(5)
                                                },
                                                (result, timeSpan, retryCount, context) =>
                                                {
                                                    this.logger.LogWarning($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                                });

            this.Policy = retryPolicy;
        }
    }
}
