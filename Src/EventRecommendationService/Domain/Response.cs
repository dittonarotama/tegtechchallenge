﻿namespace EventRecommendationService.Domain
{
    using System.Net;

    public class Response<T>
    {
        public Response()
        {
            this.Success = true;
        }

        public bool Success { get; set; }

        public T Result { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        public ErrorResponse Error { get; set; }
    }
}
