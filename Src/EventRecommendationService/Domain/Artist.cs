namespace EventRecommendationService.Domain
{
    using Newtonsoft.Json;

    public class Artist
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("match")]
        public string Match { get; set; }
    }
}