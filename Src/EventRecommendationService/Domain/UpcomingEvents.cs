namespace EventRecommendationService.Domain
{
    using System;
    using Newtonsoft.Json;

    public class UpcomingEvents
    {
        [JsonProperty("artist")]
        public string Artist { get; set; }

        [JsonProperty("eventdate")]
        public DateTime EventDate { get; set; }
    }
}
