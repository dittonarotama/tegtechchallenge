﻿namespace EventRecommendationService.Domain
{
    using System;

    public class ErrorResponse
    {
        public ErrorResponse(string message) : this(null, message)
        {
        }

        public ErrorResponse(Exception exception, string message)
        {
            this.Exception = exception;
            this.Message = message;
        }

        public Exception Exception { get; }

        public string Message { get; }
    }
}