namespace EventRecommendationService.Domain
{
    using System;
    using Newtonsoft.Json;

    public class Orders
    {
        [JsonProperty("orderid")]
        public int OrderId { get; set; }

        [JsonProperty("customerid")]
        public int CustomerId { get; set; }

        [JsonProperty("customername")]
        public string CustomerName { get; set; }

        [JsonProperty("eventid")]
        public int EventId { get; set; }

        [JsonProperty("artist")]
        public string Artist { get; set; }

        [JsonProperty("eventdate")]
        public DateTime EventDate { get; set; }
    }
}