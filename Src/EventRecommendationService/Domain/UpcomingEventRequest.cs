namespace EventRecommendationService.Domain
{
    using System.ComponentModel.DataAnnotations;

    public class UpcomingEventRequest
    {
        /// <summary>
        /// Customer Id of upcoming event based on their past order history
        /// </summary>
        [Required(ErrorMessage = "CustomerId is required")]
        public int? CustomerId { get; set; }
    }
}