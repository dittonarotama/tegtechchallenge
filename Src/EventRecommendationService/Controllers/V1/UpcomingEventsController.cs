﻿namespace EventRecommendationService.Controllers.V1
{
    using System;
    using System.Threading.Tasks;
    using Domain;
    using Domain.Api;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Processor;
    using Swashbuckle.AspNetCore.Annotations;

    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    public class UpcomingEventsController : ControllerBase
    {
        private readonly ILogger<UpcomingEventsController> logger;

        private readonly IEventRecommendationProcessor processor;

        public UpcomingEventsController(ILogger<UpcomingEventsController> logger, IEventRecommendationProcessor processor)
        {
            this.logger = logger;
            this.processor = processor;
        }

        [HttpGet]
        [Route("")]
        [SwaggerResponse(200, Type = typeof(UpcomingEvents))]
        [SwaggerResponse(400, Type = typeof(ApiErrorResponse))]
        public async Task<IActionResult> Get([FromQuery]UpcomingEventRequest request)
        {
            try
            {
                this.logger.LogInformation("getting events");

                var result = await this.processor.ProcessAsync(request);
                return this.Ok(result);
            }
            catch (InvalidOperationException exception)
            {
                this.logger.LogError(exception, exception.Message);
                return this.NotFound(new ApiErrorResponse(StatusCodes.Status404NotFound, exception.Message));
            }
            catch (Exception exception)
            {
                this.logger.LogInformation("this exception has been caught");
                this.logger.LogError(exception, exception.Message);
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    new ApiErrorResponse(StatusCodes.Status500InternalServerError, exception.Message));
            }
        }
    }
}