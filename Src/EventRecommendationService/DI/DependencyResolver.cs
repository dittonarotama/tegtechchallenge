﻿namespace EventRecommendationService.DI
{
    using System;
    using System.IO;
    using Configuration;
    using DataProvider.LastFm;
    using DataProvider.Teg;
    using Domain.Api;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Processor;
    using Proxy;

    public class DependencyResolver
    {
        private static IServiceProvider serviceProvider;

        private static bool initialised;

        private static readonly object LockObject = new object();

        public static void Init(IServiceCollection services)
        {
            if (initialised)
            {
                return;
            }

            lock (LockObject)
            {
                if (initialised)
                {
                    return;
                }

                ConfigureDependency(services);
                initialised = true;
            }
        }


        public static T GetService<T>() where T : class
        {
            return serviceProvider.GetService<T>();
        }

        private static void ConfigureDependency(IServiceCollection services)
        {
            var config = new ConfigurationBuilder().SetBasePath(Path.Combine(AppContext.BaseDirectory))
                .AddJsonFile("appsettings.json", false)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development"}.json", false)
                .Build();

            services.Configure<EventRecommendationDetails>(option => config.GetSection("EventRecommendationDetails"));

            var eventRecommendationDetails = new EventRecommendationDetails();
            config.Bind("EventRecommendationDetails", eventRecommendationDetails);
            services.AddSingleton(i => eventRecommendationDetails);

            services.AddHttpClient<IEventApiClient, EventApiClient>();
            services.AddScoped<ITegDataProvider, TegDataProvider>();
            services.AddScoped<ILastFmDataProvider, LastFmDataProvider>();
            services.AddScoped<IEventRecommendationProcessor, EventRecommendationProcessor>();
            services.AddScoped<IEventRecommendationPolicyWrapper, EventRecommendationPolicyWrapper>();

            serviceProvider = services.BuildServiceProvider();
        }
    }
}
