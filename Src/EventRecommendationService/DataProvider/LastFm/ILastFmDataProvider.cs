﻿namespace EventRecommendationService.DataProvider.LastFm
{
    using System.Threading.Tasks;
    using Domain;

    public interface ILastFmDataProvider
    {
        Task<Response<LastFmAllSimilarArtist>> GetMostSimilarArtistAsync(string artistName);
    }
}