﻿namespace EventRecommendationService.DataProvider.LastFm
{
    using System.Collections.Generic;
    using Domain;
    using Newtonsoft.Json;

    public class SimilarArtist
    {
        [JsonProperty("artist")]
        public List<Artist> Artist { get; set; }
    }
}