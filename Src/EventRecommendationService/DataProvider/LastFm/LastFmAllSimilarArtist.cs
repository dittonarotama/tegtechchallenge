﻿namespace EventRecommendationService.DataProvider.LastFm
{
    using Newtonsoft.Json;

    public class LastFmAllSimilarArtist
    {
        [JsonProperty("similarartists")]
        public SimilarArtist SimilarArtist { get; set; }

        [JsonProperty("error")]
        public int Error { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}