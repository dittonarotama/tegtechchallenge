﻿namespace EventRecommendationService.DataProvider.LastFm
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Configuration;
    using DI;
    using Domain;
    using Extension;
    using Proxy;

    public class LastFmDataProvider : ILastFmDataProvider
    {
        private readonly IEventApiClient eventApiClient;

        private EventRecommendationDetails options;

        public LastFmDataProvider(IEventApiClient eventApiClient)
        {
            this.eventApiClient = eventApiClient;
            this.options = DependencyResolver.GetService<EventRecommendationDetails>();
        }

        public async Task<Response<LastFmAllSimilarArtist>> GetMostSimilarArtistAsync(string artistName)
        {
            try
            {
                var response = await this.eventApiClient.GetAsync($"{this.options.LastFmApiBaseUri}artist.getsimilar&api_key={this.options.LastFmApiKey}&artist={artistName}&format=json&limit=1")
                    .ConfigureAwait(false);
                
                if (!response.Success)
                {
                    return new Response<LastFmAllSimilarArtist>
                    {
                        Result = null,
                        Success = false,
                        StatusCode = response.StatusCode,
                        Error = new ErrorResponse(response.Error.Message)
                    };
                }

                var contentResponse = await response.Result.Content.ReadAsStringAsync().ConfigureAwait(false);
                var deserialisedResponse = contentResponse.ToObject<LastFmAllSimilarArtist>();

                var validateResult = Enum.IsDefined(typeof(LastFmErrorTypes), deserialisedResponse.Error);
                if (validateResult)
                {
                    return new Response<LastFmAllSimilarArtist>
                    {
                        Result = null,
                        Success = false,
                        StatusCode = HttpStatusCode.OK,
                        Error = new ErrorResponse($"LastFm Error - {deserialisedResponse.Message}, Error Code: {deserialisedResponse.Error}")
                    };
                }

                return new Response<LastFmAllSimilarArtist>
                {
                    Result = deserialisedResponse,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Error = null
                };
            }
            catch (Exception exception)
            {
                return new Response<LastFmAllSimilarArtist>
                {
                    Result = null,
                    Success = false,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Error = new ErrorResponse(exception, $"Exception: {exception.Message}")
                };
            }
        }
    }
}
