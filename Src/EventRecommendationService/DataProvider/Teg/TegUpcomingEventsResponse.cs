﻿namespace EventRecommendationService.DataProvider.Teg
{
    using System.Collections.Generic;
    using Domain;

    public class TegUpcomingEventsResponse
    {
        public TegUpcomingEventsResponse()
        {
            this.UpcomingEvents = new List<UpcomingEvents>();
        }

        public List<UpcomingEvents> UpcomingEvents { get; set; }

        public string Message { get; set; }
    }
}