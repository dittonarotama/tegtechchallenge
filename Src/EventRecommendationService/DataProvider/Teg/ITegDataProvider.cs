﻿namespace EventRecommendationService.DataProvider.Teg
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Domain;

    public interface ITegDataProvider
    {
        Task<Response<IEnumerable<Orders>>> GetOrdersByCustomerIdAsync(int customerId);

        Task<Response<IEnumerable<UpcomingEvents>>> GetUpcomingEventsAsync(string artistName);
    }
}