﻿namespace EventRecommendationService.DataProvider.Teg
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using Configuration;
    using DI;
    using Domain;
    using Extension;
    using Newtonsoft.Json;
    using Proxy;

    public class TegDataProvider : ITegDataProvider
    {
        private readonly IEventApiClient eventApiClient;

        private readonly EventRecommendationDetails options;

        public TegDataProvider(IEventApiClient eventApiClient)
        {
            this.eventApiClient = eventApiClient;
            this.options = DependencyResolver.GetService<EventRecommendationDetails>();
        }

        public async Task<Response<IEnumerable<Orders>>> GetOrdersByCustomerIdAsync(int customerId)
        {
            try
            {
                var response = await this.eventApiClient.GetAsync($"{this.options.TegApiBaseUri}/orders").ConfigureAwait(false);

                if (!response.Success)
                {
                    return new Response<IEnumerable<Orders>>
                    {
                        Result = null,
                        Success = false,
                        StatusCode = response.StatusCode,
                        Error = new ErrorResponse(response.Error.Message)
                    };
                }

                var contentResponse = await response.Result.Content.ReadAsStringAsync().ConfigureAwait(false);

                var customerOrdersResult = contentResponse.ToObject<IEnumerable<Orders>>()
                                                                         .Where(resp => resp.CustomerId == customerId);

                return new Response<IEnumerable<Orders>>
                {
                    Result = customerOrdersResult,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Error = null
                };
            }
            catch (Exception exception)
            {
                return new Response<IEnumerable<Orders>>
                {
                    Result = null,
                    Success = false,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Error = new ErrorResponse(exception, $"Exception: {exception.Message}")
                };
            }
        }

        public async Task<Response<IEnumerable<UpcomingEvents>>> GetUpcomingEventsAsync(string artistName)
        {
            try
            {
                var response = await this.eventApiClient.GetAsync($"{this.options.TegApiBaseUri}/upcomingevents").ConfigureAwait(false);

                if (!response.Success)
                {
                    return new Response<IEnumerable<UpcomingEvents>>
                    {
                        Result = null,
                        Success = false,
                        StatusCode = response.StatusCode,
                        Error = new ErrorResponse(response.Error.Message)
                    };
                }

                var contentResponse = await response.Result.Content.ReadAsStringAsync().ConfigureAwait(false);

                var upcomingEventsResult = contentResponse.ToObject<IEnumerable<UpcomingEvents>>()
                                                                              .Where(resp => resp.Artist.Equals(artistName, StringComparison.InvariantCultureIgnoreCase));

                return new Response<IEnumerable<UpcomingEvents>>
                {
                    Result = upcomingEventsResult.ToList(),
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Error = null
                };
            }
            catch (Exception exception)
            {
                return new Response<IEnumerable<UpcomingEvents>>
                {
                    Result = null,
                    Success = false,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Error = new ErrorResponse(exception, $"Exception: {exception.Message}")
                };
            }
        }
    }
}
