﻿namespace EventRecommendationService.Configuration
{
    public class EventRecommendationDetails
    {
        public string TegApiBaseUri { get; set; }

        public string LastFmApiBaseUri { get; set; }

        public string LastFmApiKey { get; set; }
    }
}
