﻿namespace EventRecommendationService.Processor
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using DataProvider.LastFm;
    using DataProvider.Teg;
    using Domain;

    public class EventRecommendationProcessor : IEventRecommendationProcessor
    {
        private readonly ITegDataProvider tegDataProvider;

        private readonly ILastFmDataProvider lastFmDataProvider;

        public EventRecommendationProcessor(ILastFmDataProvider lastFmDataProvider, ITegDataProvider tegDataProvider)
        {
            this.tegDataProvider = tegDataProvider;
            this.lastFmDataProvider = lastFmDataProvider;
        }

        public async Task<Response<IEnumerable<UpcomingEvents>>> ProcessAsync(UpcomingEventRequest request)
        {
            // get customer orders
            var customerOrderResponse = await this.tegDataProvider.GetOrdersByCustomerIdAsync(request.CustomerId.Value).ConfigureAwait(false);

            if (!customerOrderResponse.Result.Any())
            {
                return new Response<IEnumerable<UpcomingEvents>>
                {
                    Result = new List<UpcomingEvents>(),
                    StatusCode = HttpStatusCode.OK,
                    Success = false,
                    Error = null
                };
            }

            // get similar artist
            var similarArtistRequest = customerOrderResponse.Result.Select(order => this.lastFmDataProvider.GetMostSimilarArtistAsync(order.Artist))
                                                                                    .ToList();

            var similarArtistResponse = await Task.WhenAll(similarArtistRequest).ConfigureAwait(false);

            var similiarArtistSuccessResponse = similarArtistResponse.Where(response => response.Success).ToList();

            if (!similiarArtistSuccessResponse.Any())
            {
                return new Response<IEnumerable<UpcomingEvents>>
                {
                    Result = new List<UpcomingEvents>(),
                    StatusCode = HttpStatusCode.OK,
                    Success = false,
                    Error = similiarArtistSuccessResponse.FirstOrDefault()?.Error
                };
            }

            var similarArtistList = similiarArtistSuccessResponse.SelectMany(response => response.Result.SimilarArtist.Artist);

            // get upcoming event
            var upcomingEventRequest = similarArtistList.Select(artist => this.tegDataProvider.GetUpcomingEventsAsync(artist.Name))
                                                                          .ToList();

            var upcomingEventsResponse = await Task.WhenAll(upcomingEventRequest).ConfigureAwait(false);

            var upcomingEventsResult = upcomingEventsResponse.SelectMany(x => x.Result);

            return new Response<IEnumerable<UpcomingEvents>>
            {
                Result = upcomingEventsResult,
                StatusCode = HttpStatusCode.OK,
                Success = true,
                Error = null
            };
        }
    }
}
