﻿namespace EventRecommendationService.Processor
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Domain;

    public interface IEventRecommendationProcessor
    {
        Task<Response<IEnumerable<UpcomingEvents>>> ProcessAsync(UpcomingEventRequest request);
    }
}