﻿namespace EventRecommendationService.Extension
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;

    public static class EventRecommendationApiVersioningExtension
    {
        public static void AddEventRecommendationApiVersioning(this IServiceCollection services)
        {
            services.AddApiVersioning(v =>
            {
                v.ReportApiVersions = true;
                v.AssumeDefaultVersionWhenUnspecified = true;
                v.DefaultApiVersion = new ApiVersion(1, 0);
            });
        }
    }
}
