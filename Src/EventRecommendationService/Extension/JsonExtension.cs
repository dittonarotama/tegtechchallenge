﻿namespace EventRecommendationService.Extension
{
    using System;
    using Newtonsoft.Json;

    public static class JsonExtension
    {
        public static T ToObject<T>(this string jsonData)
        {
            try
            {
                var result = JsonConvert.DeserializeObject<T>(jsonData, new JsonSerializerSettings
                {
                    DateTimeZoneHandling = DateTimeZoneHandling.Local
                });
                return result;
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}