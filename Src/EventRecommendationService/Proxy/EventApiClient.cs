﻿namespace EventRecommendationService.Proxy
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using Domain;
    using Domain.Api;
    using Polly;

    public class EventApiClient : IEventApiClient
    {
        private readonly HttpClient client;

        private readonly IEventRecommendationPolicyWrapper policyWrapper;

        public EventApiClient(HttpClient client, IEventRecommendationPolicyWrapper policyWrapper)
        {
            this.client = client;
            this.policyWrapper = policyWrapper;
        }

        public async Task<Response<HttpResponseMessage>> GetAsync(string requestUri)
        {
            try
            {
                this.policyWrapper.Build();
                var response = new HttpResponseMessage();

                var policyReport = await this.policyWrapper.Policy.ExecuteAndCaptureAsync(
                    async () =>
                    {
                        response = await this.client.GetAsync(requestUri).ConfigureAwait(false);
                        return response;
                    },
                    CancellationToken.None).ConfigureAwait(false);

                if (policyReport.Outcome == OutcomeType.Failure)
                {
                    return new Response<HttpResponseMessage>
                    {
                        Result = response,
                        Success = false,
                        StatusCode = response.StatusCode
                    };
                }

                return new Response<HttpResponseMessage>
                {
                    Result = response,
                    Success = true,
                    StatusCode = response.StatusCode
                };
            }
            catch (Exception exception)
            {
                return new Response<HttpResponseMessage>
                {
                    Success = false,
                    Error = new ErrorResponse(exception, exception.Message),
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }
        }
    }
}
