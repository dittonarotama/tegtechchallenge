﻿namespace EventRecommendationService.Proxy
{
    using System.Net.Http;
    using System.Threading.Tasks;
    using Domain;

    public interface IEventApiClient
    {
        Task<Response<HttpResponseMessage>> GetAsync(string requestUri);
    }
}