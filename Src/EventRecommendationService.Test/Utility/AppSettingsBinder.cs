﻿namespace EventRecommendationService.Test.Utility
{
    using DI;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class AppSettingsBinder<T>
        where T : class, new()
    {
        private readonly IConfigurationRoot config;

        public AppSettingsBinder(IConfigurationRoot configurationRoot)
        {
            DependencyResolver.Init(new ServiceCollection());
            this.config = configurationRoot;
        }

        public T GetOptions(string section)
        {
            var options = new T();

            this.config.Bind(section, options);
            return options;
        }
    }
}