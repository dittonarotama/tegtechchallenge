﻿namespace EventRecommendationService.Test.Utility
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Threading.Tasks;
    using Configuration;
    using Microsoft.Extensions.Configuration;

    public class BaseTestHelper
    {
        public BaseTestHelper()
        {
            this.Options = new AppSettingsBinder<EventRecommendationDetails>(this.GetApplicationConfig()).GetOptions("EventRecommendationDetails");
        }

        public EventRecommendationDetails Options { get; set; }

        public class FakeHttpMessageHandler : DelegatingHandler
        {
            private HttpResponseMessage fakeResponse;

            public FakeHttpMessageHandler(HttpResponseMessage responseMessage)
            {
                fakeResponse = responseMessage;
            }

            protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                return await Task.FromResult(fakeResponse);
            }
        }

        public IConfigurationRoot GetApplicationConfig()
        {
            var config = new ConfigurationBuilder().SetBasePath(Path.Combine(AppContext.BaseDirectory))
                .AddJsonFile("appsettings.json", false)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development"}.json", false)
                .Build();

            return config;
        }
    }
}
