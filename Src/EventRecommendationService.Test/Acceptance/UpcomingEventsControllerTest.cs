﻿namespace EventRecommendationService.Test.Acceptance
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Domain;
    using Domain.Api;
    using Extension;
    using FluentAssertions;
    using Microsoft.AspNetCore.Http;
    using Utility;
    using Xunit;

    public class UpcomingEventsControllerTest : BaseTestHelper
    {
        private readonly string serviceUrl;

        public UpcomingEventsControllerTest()
        {
            var config = this.GetApplicationConfig();
            this.serviceUrl = $"{config["EventRecommendationDetails:ServiceEndpoint"]}/UpcomingEvents";
        }

        [Fact]
        public async Task ShouldReturnUpcomingEventsWithValidCustomerId()
        {
            // Customer name : Alice
            var endpoint = $"{this.serviceUrl}?customerId=1001";

            using var client = new HttpClient();

            var response = await client.GetAsync(endpoint).ConfigureAwait(false);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var data = content.ToObject<Response<IEnumerable<UpcomingEvents>>>();

            data.Success.Should().BeTrue();
            foreach (var upcomingEventse in data.Result)
            {
                upcomingEventse.Artist.Should().Be("Mariah Carey");
            }
        }

        [Fact]
        public async Task ShouldReturnEmptyUpcomingEventsWithValidCustomerIdWhenThereIsNoMatchEvent()
        {
            // Customer name : Bob
            var endpoint = $"{this.serviceUrl}?customerId=1003";

            using var client = new HttpClient();

            var response = await client.GetAsync(endpoint).ConfigureAwait(false);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var data = content.ToObject<Response<IEnumerable<UpcomingEvents>>>();

            data.Success.Should().BeTrue();
            data.Result.Should().BeNullOrEmpty();
        }

        [Fact]
        public async Task ShouldReturnBadRequestUpcomingEventsWithInvalidCustomerId()
        {
            var endpoint = $"{this.serviceUrl}?customerId=";

            using var client = new HttpClient();

            var response = await client.GetAsync(endpoint).ConfigureAwait(false);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var data = content.ToObject<ApiErrorResponse>();

            data.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
            data.Message.Should().Be("Bad Request");
        }
    }
}
