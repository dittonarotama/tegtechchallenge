namespace EventRecommendationService.Test.Unit.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using DI;
    using Domain;
    using Domain.Api;
    using EventRecommendationService.Controllers.V1;
    using FluentAssertions;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Processor;
    using Utility;
    using Xunit;

    public class UpcomingEventsControllerTest : BaseTestHelper
    {
        private readonly ILogger<UpcomingEventsController> logger;

        private readonly IEventRecommendationProcessor processor;

        private readonly UpcomingEventsController controller;

        private UpcomingEventRequest request;

        public UpcomingEventsControllerTest()
        {
            this.logger = DependencyResolver.GetService<ILogger<UpcomingEventsController>>();
            this.processor = DependencyResolver.GetService<IEventRecommendationProcessor>();
            this.request = new UpcomingEventRequest
            {
                CustomerId = 1001
            };

            this.controller = new UpcomingEventsController(this.logger, this.processor);
        }

        [Fact]
        public async Task ShouldReturn200ResponseWithValidCustomerId()
        {
            var result = await controller.Get(this.request).ConfigureAwait(false);

            var okResult = result as OkObjectResult;
            var okResultResponse = okResult.Value as Response<IEnumerable<UpcomingEvents>>;

            okResult.Should().NotBe(null);
            okResult.StatusCode.Should().Be((int)HttpStatusCode.OK);

            okResultResponse.Result.Should().NotBeNullOrEmpty();
            foreach (var upcomingEvent in okResultResponse.Result)
            {
                upcomingEvent.Artist.Should().Be("Mariah Carey");
                upcomingEvent.EventDate.Should().Be(DateTime.Parse("2020-08-22T06:00:00"));
            }
        }

        [Fact]
        public async Task ShouldReturn404ResponseWithInvalidCustomerId()
        {
            this.request = new UpcomingEventRequest();

            var result = await controller.Get(this.request).ConfigureAwait(false);

            var notFoundObjectResult = result as NotFoundObjectResult;
            var notFoundResultResponse = notFoundObjectResult.Value as ApiErrorResponse; 

            notFoundObjectResult.Should().NotBe(null);
            notFoundObjectResult.StatusCode.Should().Be((int)HttpStatusCode.NotFound);

            notFoundResultResponse.Should().NotBeNull();
            notFoundResultResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
        }
    }
}
