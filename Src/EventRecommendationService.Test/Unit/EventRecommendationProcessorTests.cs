﻿namespace EventRecommendationService.Test.Unit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataProvider.LastFm;
    using DataProvider.Teg;
    using Domain;
    using FluentAssertions;
    using NSubstitute;
    using Processor;
    using Utility;
    using Xunit;

    public class EventRecommendationProcessorTests : BaseTestHelper
    {
        private readonly IEventRecommendationProcessor processor;

        private readonly ILastFmDataProvider lastFmDataProvider;

        private readonly ITegDataProvider tegDataProvider;

        private readonly UpcomingEventRequest request;

        public EventRecommendationProcessorTests()
        {
            this.lastFmDataProvider = Substitute.For<ILastFmDataProvider>();
            this.tegDataProvider = Substitute.For<ITegDataProvider>();

            this.request = new UpcomingEventRequest
            {
                CustomerId = 1001
            };

            this.processor = new EventRecommendationProcessor(this.lastFmDataProvider, this.tegDataProvider);
        }

        [Fact]
        public async Task ShouldReturnUpcomingEventsRecommendation()
        {
            var tegCustomerOrderResponse = new Response<IEnumerable<Orders>>
            {
                Result = new List<Orders>
                {
                    new Orders
                    {
                        Artist = "Janet Jackson",
                        CustomerName = "Alice",
                        CustomerId = 1001,
                        EventDate = new DateTime(2016, 10, 10, 19, 0, 0, DateTimeKind.Utc)
                    }
                }
            };

            this.tegDataProvider.GetOrdersByCustomerIdAsync(1001).Returns(tegCustomerOrderResponse);

            var lastFmSimilarArtistResponse = new Response<LastFmAllSimilarArtist>
            {
                Result = new LastFmAllSimilarArtist
                {
                    SimilarArtist = new SimilarArtist
                    {
                        Artist = new List<Artist>
                        {
                            new Artist
                            {
                                Name = "Mariah Carey",
                                Match = "1"
                            }
                        }
                    }
                }
            };

            this.lastFmDataProvider.GetMostSimilarArtistAsync(tegCustomerOrderResponse.Result.First().Artist)
                                    .Returns(lastFmSimilarArtistResponse);

            var tegUpcomingEventsResponse = new Response<IEnumerable<UpcomingEvents>>
            {
                Result = new List<UpcomingEvents>
                {
                    new UpcomingEvents
                    {
                        Artist = "Mariah Carey",
                        EventDate = new DateTime(2020, 08, 23, 20, 0,0, DateTimeKind.Utc)
                    }
                }
            };

            this.tegDataProvider.GetUpcomingEventsAsync(lastFmSimilarArtistResponse.Result.SimilarArtist.Artist.First().Name)
                                    .Returns(tegUpcomingEventsResponse);


            var processorResponse = await this.processor.ProcessAsync(this.request).ConfigureAwait(false);

            foreach (var upcomingEvent in processorResponse.Result)
            {
                upcomingEvent.Artist.Should().BeOfType<string>();
                upcomingEvent.EventDate.GetType().Should().Be(typeof(DateTime));

                upcomingEvent.Artist.Should().Be("Mariah Carey");
            }

            await this.tegDataProvider.Received(1).GetOrdersByCustomerIdAsync(1001).ConfigureAwait(false);
            await this.lastFmDataProvider.Received(1).GetMostSimilarArtistAsync(Arg.Any<string>()).ConfigureAwait(false);
            await this.tegDataProvider.Received(1).GetUpcomingEventsAsync(Arg.Any<string>());
        }

        [Fact]
        public async Task ShouldNotReturnUpcomingEventsWhenThereIsNoCustomerOrder()
        {
            var tegCustomerOrderResponse = new Response<IEnumerable<Orders>>
            {
                Result = new List<Orders>()
            };

            this.tegDataProvider.GetOrdersByCustomerIdAsync(1001).Returns(tegCustomerOrderResponse);

            var processorResponse = await this.processor.ProcessAsync(this.request).ConfigureAwait(false);

            foreach (var upcomingEvent in processorResponse.Result)
            {
                upcomingEvent.Artist.Should().BeOfType<string>();
                upcomingEvent.EventDate.GetType().Should().Be(typeof(DateTime));

                upcomingEvent.Artist.Should().Be(string.Empty);
            }

            await this.tegDataProvider.Received(1).GetOrdersByCustomerIdAsync(1001).ConfigureAwait(false);
            await this.lastFmDataProvider.Received(0).GetMostSimilarArtistAsync(Arg.Any<string>()).ConfigureAwait(false);
            await this.tegDataProvider.Received(0).GetUpcomingEventsAsync(Arg.Any<string>());
        }

        [Fact]
        public async Task ShouldNotReturnUpcomingEventsWhenThereIsNoMatchLastFmSimiliarArtist()
        {
            var tegCustomerOrderResponse = new Response<IEnumerable<Orders>>
            {
                Result = new List<Orders>
                {
                    new Orders
                    {
                        Artist = "Janet Jackson",
                        CustomerName = "Alice",
                        CustomerId = 1001,
                        EventDate = new DateTime(2016, 10, 10, 19, 0, 0, DateTimeKind.Utc)
                    }
                }
            };

            this.tegDataProvider.GetOrdersByCustomerIdAsync(1001).Returns(tegCustomerOrderResponse);

            var lastFmSimilarArtistResponse = new Response<LastFmAllSimilarArtist>
            {
                Result = new LastFmAllSimilarArtist
                {
                    SimilarArtist = null,
                    Error = 6,
                    Message = "The artist you supplied could not be found"
                },
                Success = false
            };

            this.lastFmDataProvider.GetMostSimilarArtistAsync(tegCustomerOrderResponse.Result.First().Artist)
                .Returns(lastFmSimilarArtistResponse);

            var processorResponse = await this.processor.ProcessAsync(this.request).ConfigureAwait(false);

            foreach (var upcomingEvent in processorResponse.Result)
            {
                upcomingEvent.Artist.Should().BeOfType<string>();
                upcomingEvent.EventDate.GetType().Should().Be(typeof(DateTime));

                upcomingEvent.Artist.Should().Be(string.Empty);
            }

            await this.tegDataProvider.Received(1).GetOrdersByCustomerIdAsync(1001).ConfigureAwait(false);
            await this.lastFmDataProvider.Received(1).GetMostSimilarArtistAsync(Arg.Any<string>()).ConfigureAwait(false);
            await this.tegDataProvider.Received(0).GetUpcomingEventsAsync(Arg.Any<string>());
        }

        [Fact]
        public async Task ShouldNotReturnUpcomingEventsWhenThereIsNoMatchTegUpcomingEvent()
        {
            var tegCustomerOrderResponse = new Response<IEnumerable<Orders>>
            {
                Result = new List<Orders>
                {
                    new Orders
                    {
                        Artist = "Janet Jackson",
                        CustomerName = "Alice",
                        CustomerId = 1001,
                        EventDate = new DateTime(2016, 10, 10, 19, 0, 0, DateTimeKind.Utc)
                    }
                }
            };

            this.tegDataProvider.GetOrdersByCustomerIdAsync(1001).Returns(tegCustomerOrderResponse);

            var lastFmSimilarArtistResponse = new Response<LastFmAllSimilarArtist>
            {
                Result = new LastFmAllSimilarArtist
                {
                    SimilarArtist = new SimilarArtist
                    {
                        Artist = new List<Artist>
                        {
                            new Artist
                            {
                                Name = "MariahCarey",
                                Match = "1"
                            }
                        }
                    }
                }
            };

            this.lastFmDataProvider.GetMostSimilarArtistAsync(tegCustomerOrderResponse.Result.First().Artist)
                                    .Returns(lastFmSimilarArtistResponse);

            var tegUpcomingEventsResponse = new Response<IEnumerable<UpcomingEvents>>
            {
                Result = new List<UpcomingEvents>()
            };

            this.tegDataProvider.GetUpcomingEventsAsync(lastFmSimilarArtistResponse.Result.SimilarArtist.Artist.First().Name)
                                    .Returns(tegUpcomingEventsResponse);


            var processorResponse = await this.processor.ProcessAsync(this.request).ConfigureAwait(false);

            processorResponse.Result.Should().BeNullOrEmpty();

            await this.tegDataProvider.Received(1).GetOrdersByCustomerIdAsync(1001).ConfigureAwait(false);
            await this.lastFmDataProvider.Received(1).GetMostSimilarArtistAsync(Arg.Any<string>()).ConfigureAwait(false);
            await this.tegDataProvider.Received(1).GetUpcomingEventsAsync(Arg.Any<string>());
        }
    }
}