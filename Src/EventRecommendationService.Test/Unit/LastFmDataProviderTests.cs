﻿namespace EventRecommendationService.Test.Unit
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Configuration;
    using DataProvider.LastFm;
    using DI;
    using Domain;
    using FluentAssertions;
    using NSubstitute;
    using NSubstitute.ExceptionExtensions;
    using Proxy;
    using Utility;
    using Xunit;

    public class LastFmDataProviderTests : BaseTestHelper
    {
        private readonly ILastFmDataProvider lastFmDataProvider;

        private readonly IEventApiClient eventApiClient;

        private readonly EventRecommendationDetails options;

        public LastFmDataProviderTests()
        {
            this.eventApiClient = Substitute.For<IEventApiClient>();
            this.options = DependencyResolver.GetService<EventRecommendationDetails>();

            this.lastFmDataProvider = new LastFmDataProvider(this.eventApiClient);
        }

        [Fact]
        public async Task ShouldReturnGetMostSimilarArtist()
        {
            var jsonResponse = File.ReadAllText(@".\SampleResults\LastFmApiResponse.json");
            var testResponse = new Response<HttpResponseMessage>
            {
                Result = new HttpResponseMessage
                {
                    Content = new StringContent(jsonResponse)
                },
                Success = true
            };
            this.eventApiClient.GetAsync(Arg.Any<string>()).Returns(testResponse);

            var response = await lastFmDataProvider.GetMostSimilarArtistAsync("janet jackson").ConfigureAwait(false);

            response.GetType().Should().Be(typeof(Response<LastFmAllSimilarArtist>));
            response.Result.SimilarArtist.Artist.Should().NotBeNullOrEmpty();
            response.Success.Should().BeTrue();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Error.Should().BeNull();

            await this.eventApiClient.Received(1).GetAsync(Arg.Is<string>(x =>
                                                                x.Equals($"{this.options.LastFmApiBaseUri}artist.getsimilar&api_key={this.options.LastFmApiKey}&artist=janet jackson&format=json&limit=1")));
        }

        [Fact]
        public async Task ShouldNotReturnGetMostSimilarArtist()
        {
            var testResponse = new Response<HttpResponseMessage>
            {
                Success = false,
                Error = new ErrorResponse("error"),
                StatusCode = HttpStatusCode.BadRequest
            };
            this.eventApiClient.GetAsync(Arg.Any<string>()).Returns(testResponse);

            var response = await this.lastFmDataProvider.GetMostSimilarArtistAsync("janet jackson").ConfigureAwait(false);
            
            response.GetType().Should().Be(typeof(Response<LastFmAllSimilarArtist>));
            response.Result.Should().BeNull();
            response.Success.Should().BeFalse();
            response.StatusCode = HttpStatusCode.BadRequest;
            response.Error.Message.Should().Be("error");
        }

        [Fact]
        public async Task ShouldNotReturnGetMostSimilarArtistIfThrowException()
        {
            this.eventApiClient.GetAsync(Arg.Any<string>()).Throws(new Exception("Request failed for some reason"));

            var response = await this.lastFmDataProvider.GetMostSimilarArtistAsync("janet jackson").ConfigureAwait(false);

            response.GetType().Should().Be(typeof(Response<LastFmAllSimilarArtist>));
            response.Result.Should().BeNull();
            response.Success.Should().BeFalse();
            response.StatusCode = HttpStatusCode.BadRequest;
            response.Error.Message.Should().Be("Exception: Request failed for some reason");
        }
    }
}