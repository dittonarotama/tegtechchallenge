﻿namespace EventRecommendationService.Test.Unit
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Configuration;
    using DataProvider.Teg;
    using DI;
    using Domain;
    using FluentAssertions;
    using NSubstitute;
    using NSubstitute.ExceptionExtensions;
    using Proxy;
    using Utility;
    using Xunit;

    public class TegDataProviderTests : BaseTestHelper
    {
        private const int CustomerId = 1001;

        private readonly ITegDataProvider tegDataProvider;

        private readonly IEventApiClient eventApiClient;

        private readonly EventRecommendationDetails options;

        public TegDataProviderTests()
        {
            this.eventApiClient = Substitute.For<IEventApiClient>();
            this.options = DependencyResolver.GetService<EventRecommendationDetails>();

            this.tegDataProvider = new TegDataProvider(this.eventApiClient);
        }

        [Fact]
        public async Task ShouldReturnCustomerOrders()
        {
            var jsonResponse = File.ReadAllText(@".\SampleResults\TegOrdersApiResponse.json");
            var testResponse = new Response<HttpResponseMessage>
            {
                Result = new HttpResponseMessage
                {
                    Content = new StringContent(jsonResponse)
                },
                Success = true
            };
            this.eventApiClient.GetAsync(Arg.Any<string>()).Returns(testResponse);

            var response = await this.tegDataProvider.GetOrdersByCustomerIdAsync(CustomerId).ConfigureAwait(false);

            response.Result.Should().NotBeNullOrEmpty();
            response.Should().BeOfType<Response<IEnumerable<Orders>>>();
            foreach (var tegOrdersResponse in response.Result)
            {
                tegOrdersResponse.CustomerName.Should().Be("Alice");
            }
        }

        [Fact]
        public async Task ShouldNotReturnCustomerOrders()
        {
            var testResponse = new Response<HttpResponseMessage>
            {
                Success = false,
                Error = new ErrorResponse("error")
            };
            this.eventApiClient.GetAsync(Arg.Any<string>()).Returns(testResponse);

            var response = await this.tegDataProvider.GetOrdersByCustomerIdAsync(CustomerId).ConfigureAwait(false);
            response.Should().NotBeNull();
            response.Error.Message.Should().Be("error");
        }

        [Fact]
        public async Task ShouldNotReturnCustomerOrdersIfThrowException()
        {
            this.eventApiClient.GetAsync(Arg.Any<string>()).Throws(new Exception("Request failed for some reason"));

            var response = await this.tegDataProvider.GetOrdersByCustomerIdAsync(CustomerId).ConfigureAwait(false);
            response.Should().NotBeNull();
            response.Result.Should().BeNullOrEmpty();
            response.Error.Message.Should().Be("Exception: Request failed for some reason");
        }

        [Fact]
        public async Task ShouldReturnUpcomingEvents()
        {
            var jsonResponse = File.ReadAllText(@".\SampleResults\TegUpcomingEventsResponse.json");
            var testResponse = new Response<HttpResponseMessage>
            {
                Result = new HttpResponseMessage
                {
                    Content = new StringContent(jsonResponse)
                },
                Success = true
            };
            this.eventApiClient.GetAsync(Arg.Any<string>()).Returns(testResponse);

            var response = await this.tegDataProvider.GetUpcomingEventsAsync("mariah carey").ConfigureAwait(false);

            response.Result.Should().NotBeNullOrEmpty();
            response.Should().BeOfType<Response<IEnumerable<UpcomingEvents>>>();
            foreach (var tegUpcomingResponse in response.Result)
            {
                tegUpcomingResponse.Artist.Should().Be("Mariah Carey");
            }
        }

        [Fact]
        public async Task ShouldNotReturnUpcomingEvents()
        {
            var testResponse = new Response<HttpResponseMessage>
            {
                Success = false,
                Error = new ErrorResponse("error")
            };
            this.eventApiClient.GetAsync(Arg.Any<string>()).Returns(testResponse);

            var response = await this.tegDataProvider.GetUpcomingEventsAsync("c").ConfigureAwait(false);
            response.Should().NotBeNull();
            response.Error.Message.Should().Be("error");
        }

        [Fact]
        public async Task ShouldNotReturnUpcomingEventsIfThrowException()
        {
            this.eventApiClient.GetAsync(Arg.Any<string>()).Throws(new Exception("Request failed for some reason"));

            var response = await this.tegDataProvider.GetUpcomingEventsAsync("mariah carey").ConfigureAwait(false);
            response.Should().NotBeNull();
            response.Result.Should().BeNullOrEmpty();
            response.Error.Message.Should().Be("Exception: Request failed for some reason");
        }
    }
}