﻿namespace EventRecommendationService.Test.Unit
{
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using DI;
    using Domain.Api;
    using FluentAssertions;
    using Microsoft.AspNetCore.Http;
    using NSubstitute;
    using Proxy;
    using Utility;
    using Xunit;

    public class EventApiClientTests : BaseTestHelper
    {
        private IEventApiClient client;

        public EventApiClientTests()
        {
            var policyWrapper = DependencyResolver.GetService<IEventRecommendationPolicyWrapper>();
            var httpClientFactoryMock = Substitute.For<IHttpClientFactory>();

            var stringResponse = File.ReadAllText(@".\SampleResults\TegOrdersApiResponse.json");
            var fakeHttpMessageHandler = new FakeHttpMessageHandler(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(stringResponse, Encoding.UTF8, "application/json")
            });
            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);

            httpClientFactoryMock.CreateClient().Returns(fakeHttpClient);

            this.client = new EventApiClient(fakeHttpClient, policyWrapper);
        }

        [Fact]
        public async Task ShouldReturnSuccessResponseMessageGetAsync()
        {
            var response = await this.client.GetAsync("https://tegtech.docs.apiary.io/orders").ConfigureAwait(false);

            response.Should().NotBeNull();
            response.Success.Should().BeTrue();
            response.Result.GetType().Should().Be(typeof(HttpResponseMessage));
            response.Result.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task ShouldNotReturnSuccessResponseMessageGetAsync()
        {
            this.client = DependencyResolver.GetService<IEventApiClient>();
            var response = await this.client.GetAsync("https://private-9351b-tegtech.apiary-mock.com/order").ConfigureAwait(false);

            response.Should().NotBeNull();
            response.Success.Should().BeFalse();
            response.Result.Should().NotBeNull();
            response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
        }
    }
}
