# Event Recommendation Service

## Synopsis
This project is an Event Recommendation Service Api. It provides information about upcoming music events based on customer historical order.

This project consists of Asp.net core project and test project. It uses swagger for Api documentation (https://localhost:44396/swagger/index.html).

There is only one GET endpoint under UpcomingEventsController which accepts customerId as part of the request.

It returns 200 Ok http code for success call and 400 bad request for invalid parameter. It also returns 404 and 500 as part of exception handling.
the 400 bad request is handled in custom action filter attribute (ValidateRequestAttribute);

This Api service consumes ticketek endpoints to get customer orders and upcoming events. It also calls LastFm Api to get similar artist.

This project implements circuit breaker retry pattern on the calls to the external Apis.


Workflow:
1. Initially this api service calls ticketek orders endpoint to get customer past orders history.
2. From the past customer order result, requests are made to LastFm Api to get similar artist.
3. It calls ticketek upcomingEvents endpoint to match the similar artist result.

### External Api
- https://private-9351b-tegtech.apiary-mock.com/orders
- https://private-9351b-tegtech.apiary-mock.com/upcomingevents 
- https://www.last.fm/api/show/artist.getSimilar

### Development

windows only

### Installation

`
cd "tegtechchallenge\Src"
`
`
dotnet restore
`

Set environment variable for Acceptance Tests to run
`
Set-Item -Path Env:ASPNETCORE_ENVIRONMENT -Value "Development"
`

### Tests
Testing framework: XUnit
Mocking framework: NSubstitute
Assertion framework: FluentAssertions

`
cd "tegtechchallenge\Src\EventRecommendationService.Test"
`
`
dotnet restore
dotnet test
`

### Contributors
ditto.narotama@gmail.com